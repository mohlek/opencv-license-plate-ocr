#!/bin/env python3

import cv2

classNames = { 	0: 'background',
            	1: 'aeroplane', 2: 'bicycle', 3: 'bird', 4: 'boat',
                5: 'bottle', 6: 'bus', 7: 'car', 8: 'cat', 9: 'chair',
                10: 'cow', 11: 'diningtable', 12: 'dog', 13: 'horse',
                14: 'motorbike', 15: 'person', 16: 'pottedplant',
                17: 'sheep', 18: 'sofa', 19: 'train', 20: 'tvmonitor' }

net = cv2.dnn.readNetFromCaffe('deploy.prototxt', 'mobilenet_iter_73000.caffemodel')
capture = cv2.VideoCapture('IMG_0113.mp4')

divx = cv2.VideoWriter_fourcc(*'XVID')
out = cv2.VideoWriter('output.avi', divx, 15.0, (960, 540))

while capture.isOpened() :
    ret, frame = capture.read()
   
    # resize images for preview and network
    frame_resized_window = cv2.resize(frame, (960, 540)) 
    frame_resized = cv2.resize(frame_resized_window, (300, 300))

    # create blog for network
    blob = cv2.dnn.blobFromImage(frame_resized, 0.007843, (300, 300), (127.5, 127.5, 127.5), False)
	
    net.setInput(blob)
    detections = net.forward()

    cols = frame_resized.shape[1]
    rows = frame_resized.shape[0]
    
    heightFactor = frame_resized_window.shape[0] / 300.0
    widthFactor = frame_resized_window.shape[1] / 300.0

    for i in range(detections.shape[2]) :
        confidence = detections[0, 0, i, 2]
        if confidence > 0.3 :
            class_id = int(detections[0, 0, i, 1])

            xLeftBottom = int(detections[0, 0, i, 3] * cols * widthFactor)
            yLeftBottom = int(detections[0, 0, i, 4] * rows * heightFactor)
            xRightTop =   int(detections[0, 0, i, 5] * cols * widthFactor)
            yRightTop =   int(detections[0, 0, i, 6] * rows * heightFactor)

            cv2.rectangle(frame_resized_window, (xLeftBottom, yLeftBottom), (xRightTop, yRightTop), (0, 255, 0))
            cv2.putText(frame_resized_window, classNames[class_id] + " : " + str(confidence), (xLeftBottom, yLeftBottom), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0))

    # show image
    cv2.imshow('image', frame_resized_window)
    out.write(frame_resized_window)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

capture.release()
out.release()
cv2.destroyAllWindows()
